FROM python:3.6-alpine

RUN apk add curl gcc musl-dev mariadb-connector-c-dev openldap-dev curl bash libffi-dev

WORKDIR /usr/local/bin
RUN curl -L -o wait-for-it.sh https://github.com/vishnubob/wait-for-it/raw/master/wait-for-it.sh
RUN chmod +x wait-for-it.sh

RUN mkdir /code
WORKDIR /code
ADD . /code

RUN pip install pipenv
RUN pipenv install

ENV PORT 8000
EXPOSE $PORT

ENTRYPOINT pipenv run python manage.py runserver 0.0.0.0:${PORT}