from django.contrib.auth.models import User, Group
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from api.serializers import UserSerializer, GroupSerializer, MemberSerializer, SvfGroupSerializer, MembershipSerializer
from api.models import Member, SvfGroup, Membership


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class MemberViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows members to be viewed or edited.
    """
    queryset = Member.objects.all()
    serializer_class = MemberSerializer


class SvfGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows svf groups to be viewed or edited.
    """
    queryset = SvfGroup.objects.all().filter(active=True)
    serializer_class = SvfGroupSerializer

    @action(detail=True, methods=['GET'])
    def archive(self, request, pk=None):
        group = get_object_or_404(SvfGroup, pk=pk)
        group.active = False
        group.save()
        return Response({'status': 'group archived'})


class MembershipViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows svf memberships to be viewed or edited.
    """
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
