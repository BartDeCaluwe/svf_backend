# Generated by Django 2.1.5 on 2019-01-13 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20190113_1344'),
    ]

    operations = [
        migrations.AddField(
            model_name='svfgroup',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
