from django.contrib.auth.models import User, Group
from api.models import Member, SvfGroup, Membership
from rest_framework import serializers
from django.db import transaction


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'name', 'email')


class MembershipSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='member.id')
    membership_id = serializers.ReadOnlyField(source='id')
    group_id = serializers.ReadOnlyField(source='group.id')
    name = serializers.ReadOnlyField(source='member.name')
    email = serializers.ReadOnlyField(source='member.email')

    class Meta:
        model = Membership
        depth = 1
        fields = ('id', 'group_id', 'membership_id',
                  'name', 'email', 'fee_is_payed')


class SvfGroupSerializer(serializers.ModelSerializer):
    members = MembershipSerializer(source='membership_set', many=True)

    class Meta:
        model = SvfGroup
        depth = 1
        fields = ('id', 'name', 'description', 'members')

    def update(self, instance, validated_data):
        # Ignore the fact that I delete and replace. Will diff in the future
        Membership.objects.filter(group=instance).delete()
        members = self.initial_data.get("members")
        for member_id in members:
            new_member = Member.objects.get(pk=member_id)
            Membership(group=instance, member=new_member).save()

        instance.__dict__.update(**validated_data)
        instance.save()
        return instance

    def create(self, validated_data):
        validated_data.pop("membership_set")
        group = SvfGroup.objects.create(**validated_data)
        members = self.initial_data['members']
        if members:
            for member in members:
                member_instance = Member.objects.get(pk=member['id'])
                Membership(group=group, member=member_instance,
                           ).save()
        group.save()
        return group
