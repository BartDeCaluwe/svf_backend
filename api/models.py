from django.db import models


class Member(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()

    class Meta:
        ordering = ('name',)
        unique_together = ('name', 'email')


class SvfGroup(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    members = models.ManyToManyField(
        Member, through='Membership', blank=True)

    class Meta:
        ordering = ('name',)


class Membership(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    group = models.ForeignKey(SvfGroup, on_delete=models.CASCADE)
    fee_is_payed = models.BooleanField(default=False)
